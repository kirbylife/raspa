pub trait IntoUrl {
    fn into_url(self) -> Result<url::Url, url::ParseError>;
}

impl IntoUrl for url::Url {
    fn into_url(self) -> Result<url::Url, url::ParseError> {
        Ok(self)
    }
}

impl<'a> IntoUrl for &'a str {
    fn into_url(self) -> Result<url::Url, url::ParseError> {
        url::Url::parse(self)
    }
}

impl<'a> IntoUrl for &'a String {
    fn into_url(self) -> Result<url::Url, url::ParseError> {
        (&**self).into_url()
    }
}

impl IntoUrl for String {
    fn into_url(self) -> Result<url::Url, url::ParseError> {
        (&*self).into_url()
    }
}
