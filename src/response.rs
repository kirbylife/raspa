use http::StatusCode;
use serde::de::DeserializeOwned;
use serde_json::Result;
use url::Url;

use crate::selector::SelectorBase;

#[derive(Debug)]
pub struct Response {
    pub text: String,
    pub status_code: StatusCode,
    pub url: Url,
}

impl SelectorBase for Response {
    fn from_html<S: AsRef<str>>(_: S) -> Self {
        unimplemented!()
    }

    fn html(&self) -> String {
        self.text.clone()
    }
}

impl Response {
    pub fn to_json<T: DeserializeOwned>(&self) -> Result<T> {
        let plain_text: &[u8] = self.text.as_ref();
        let json = serde_json::from_slice(plain_text)?;
        Ok(json)
    }
}
