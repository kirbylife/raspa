use std::env;

fn main() {
    println!("cargo:rerun-if-env-changed={}", "DEFAULT_USER_AGENT");

    let user_agent = env::var("DEFAULT_USER_AGENT").unwrap_or(format!(
        "Mozilla/5.0 ({}) Raspa/{}",
        std::env::consts::OS,
        env!("CARGO_PKG_VERSION")
    ));

    println!("cargo:rustc-env=DEFAULT_USER_AGENT={}", user_agent);
}
